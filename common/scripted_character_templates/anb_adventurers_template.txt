﻿anb_average_adventurer_character = {
	age = { 18 40 }
	faith = root.faith
	culture = root.culture
	gender_female_chance = 50 # No gender blocks for adventuring - TODO, maybe there should be
	random_traits_list = {
		count = 1
		# Fighter or Barbarians
		education_martial_1= { weight = { base = 10 } }
		education_martial_2 = { weight = { base = 10 } }
		education_martial_3 = { weight = { base = 10 } }
		education_martial_4 = { weight = { base = 10 } }
		
		# Clerics and wizard types
		education_learning_1 = { weight = { base = 7 } }
		education_learning_2 = { weight = { base = 7 } }
		education_learning_3 = { weight = { base = 7 } }
		education_learning_4 = { weight = { base = 7 } }
		
		# Rogues
		education_intrigue_1 = { weight = { base = 5 } }
		education_intrigue_2 = { weight = { base = 5 } }
		education_intrigue_3 = { weight = { base = 5 } }
		education_intrigue_4 = { weight = { base = 5 } }
		
		# Bards
		education_diplomacy_1 = {
			weight = {
				base = 2
				modifier = {
					add = 3
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
		education_diplomacy_2 = {
			weight = {
				base = 2
				modifier = {
					add = 3
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
		education_diplomacy_3 = {
			weight = {
				base = 2
				modifier = {
					add = 3
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
		education_diplomacy_4 = {
			weight = {
				base = 2
				modifier = {
					add = 3
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
	}
	random_traits_list = {
		count = 3
		brave = {}
		just = {}
		lustful = {}
		chaste = {}
		wrathful = {}
		arrogant = {}
		impatient = {}
		patient = {}
		humble = {}
		calm = {}
		honest = {}
		ambitious = {}
		cynical = {}
		zealous = {}
		compassionate = {}
		stubborn = {}
		generous = {}
		sadistic = {}
		vengeful = {}
	}
	# "Class" traits
	random_traits_list = {
		count = { 0 1 } # Not everyone would be skilled at their job, so some just don't have the trait
		lifestyle_blademaster = {}
		lifestyle_hunter = {}
		strategist = {}
		gallant = {}
		torturer = {}
		lifestyle_mystic = {}
		peasant_leader = {}
		#order_member = {} # Does not work for reason
		berserker = {}
		poet = {} # Bards
	}
	# Origin/Personality traits
	random_traits_list = {
		count = { 0 1 }
		drunkard = {}
		irritable = {}
		athletic = {}
		strong = {}
		shrewd = {}
		clubfooted = {}
		hunchbacked = {}
		lisping = {}
		stuttering = {}
		giant = {}
		dull = {}
		albino = {}
		wheezing = {}
		beauty_bad_1 = {}
		beauty_bad_2 = {}
		beauty_bad_3 = {}
		beauty_good_1 = {}
		beauty_good_2 = {}
		beauty_good_3 = {}
		intellect_bad_1 = {}
		intellect_bad_2 = {}
		intellect_good_1 = {}
		intellect_good_2 = {}
		physique_good_1 = {}
		physique_good_2 = {}
		physique_good_3 = {}
		kinslayer_1 = {}
		bastard = {
			trigger = {
				root.faith = { NOT = { has_doctrine = doctrine_bastardry_none } }
			}
		}
	}
	random_traits = no
	martial = {
		min_template_average_skill
		max_template_average_skill
	}
	prowess = { 5 12 }

	after_creation = {
		random_list = {
			75 = {}
			25 = {
				add_trait = adventurer
				add_prestige = 500
				add_prestige_level = 1
			}
		}
		#Anbennar
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
	}
}

anb_good_adventurer_character = {
	age = { 24 40 } # Older cause they have to be established
	faith = root.faith
	culture = root.culture
	gender_female_chance = 50
	random_traits_list = {
		count = 1
		# Fighter or Barbarians
		education_martial_1= { weight = { base = 5 } }
		education_martial_2 = { weight = { base = 5 } }
		education_martial_3 = { weight = { base = 10 } }
		education_martial_4 = { weight = { base = 10 } }
		
		# Clerics and wizard types
		education_learning_1 = { weight = { base = 3 } }
		education_learning_2 = { weight = { base = 3 } }
		education_learning_3 = { weight = { base = 7 } }
		education_learning_4 = { weight = { base = 7 } }
		
		# Rogues
		education_intrigue_1 = { weight = { base = 2 } }
		education_intrigue_2 = { weight = { base = 2 } }
		education_intrigue_3 = { weight = { base = 5 } }
		education_intrigue_4 = { weight = { base = 5 } }
		
		# Bards
		education_diplomacy_1 = {
			weight = {
				base = 1
				modifier = {
					add = 1
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
		education_diplomacy_2 = {
			weight = {
				base = 1
				modifier = {
					add = 1
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
		education_diplomacy_3 = {
			weight = {
				base = 2
				modifier = {
					add = 3
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
		education_diplomacy_4 = {
			weight = {
				base = 2
				modifier = {
					add = 3
					culture = { has_cultural_parameter = poet_trait_more_common }
				}
			}
		}
	}
	random_traits_list = {
		count = 3
		brave = {}
		just = {}
		lustful = {}
		chaste = {}
		wrathful = {}
		arrogant = {}
		impatient = {}
		patient = {}
		humble = {}
		calm = {}
		honest = {}
		ambitious = {}
		cynical = {}
		zealous = {}
		compassionate = {}
		stubborn = {}
		generous = {}
		sadistic = {}
		vengeful = {}
	}
	# "Class" traits
	random_traits_list = {
		count = 1
		lifestyle_blademaster = {}
		lifestyle_hunter = {}
		strategist = {}
		gallant = {}
		torturer = {}
		lifestyle_mystic = {}
		peasant_leader = {}
		berserker = {}
		#order_member = {} # Does not work for some reason
		poet = {} # Bards
	}
	# Origin/Personality traits - Need to add more good ones so there's fewer inheritable being picked
	random_traits_list = {
		count = { 0 1 }
		drunkard = {}
		irritable = {}
		athletic = {}
		strong = {}
		shrewd = {}
		clubfooted = {}
		hunchbacked = {}
		lisping = {}
		stuttering = {}
		giant = {}
		albino = {}
		beauty_bad_1 = {}
		beauty_bad_2 = {}
		beauty_bad_3 = {}
		beauty_good_1 = {}
		beauty_good_2 = {}
		beauty_good_3 = {}
		intellect_bad_1 = {}
		intellect_bad_2 = {}
		intellect_good_1 = {}
		intellect_good_2 = {}
		physique_good_1 = {}
		physique_good_2 = {}
		physique_good_3 = {}
		kinslayer_1 = {}
		bastard = {
			trigger = {
				root.faith = { NOT = { has_doctrine = doctrine_bastardry_none } }
			}
		}
	}
	random_traits = no
	martial = {
		min_template_average_skill
		max_template_average_skill
	}
	prowess = { 10 14 }

	after_creation = {
		add_trait = adventurer
		add_prestige = 200
		add_prestige_level = 3

		#Anbennar
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
		
		# Add order members after cause it doesn't work properly
		random_list = {
			95 = {}
			5 = {
				add_trait = order_member
			}
		}
	}
}