﻿
the_moormen_kneel_decision_effect = {
	culture:moorman = {
		if = {
			limit = {
				has_cultural_tradition = tradition_staunch_traditionalists
			}
			remove_culture_tradition = tradition_staunch_traditionalists
		}
		else_if = {
			limit = {
				has_cultural_tradition = tradition_collective_lands
			}
			remove_culture_tradition = tradition_collective_lands
		}
		add_culture_tradition = tradition_loyal_soldiers
	}
	add_dynasty_prestige = 300
}
