﻿#Rihouen of Toarnen
50000 = {
	name = "Rihouen"
	religion = court_of_nerat
	culture = roilsardi
	dynasty = dynasty_toarnen
	
	trait = race_human
	trait = education_martial_2
	trait = patient
	trait = brave
	trait = arrogant
	trait = twin
	trait = human_purist
	
	father = 50004
	mother = 50005

	996.1.30 = {
		birth = yes
	}
	1020.4.8 = {
		add_spouse = 50001
	}
}

#Guencenedl
50001 = {
	name = "Guencenedl"
	religion = court_of_nerat
	culture = roilsardi
	female = yes
	
	trait = patient
	trait = forgiving
	trait = cynical
	trait = education_stewardship_2

	998.5.25 = {
		birth = yes
	}
	1020.4.8 = {
		add_spouse = 50000
	}
}

#Stefen of Celliande
50002 = {
	name = "Stefen"
	religion = court_of_nerat
	culture = roilsardi
	dynasty = dynasty_celliande
	sexuality = heterosexual
	father = 50004
	mother = 50005
	
	trait = compassionate
	trait = brave
	trait = lustful
	trait = twin
	trait = education_martial_2

	996.1.30 = {
		birth = yes
	}
	1020.6.6 = {
		add_spouse = 50003
	}
}

#Sibilia of Irmathmas
50003 = {
	name = "Sibilia"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_kobali
	sexuality = heterosexual
	female = yes
	
	father = 50006
	mother = 50007
	
	trait = shy
	trait = humble
	trait = compassionate
	trait = education_learning_2

	997.4.12 = {
		birth = yes
	}
	1021.8.6 = {
		add_spouse = 50002
		add_pressed_claim = title:c_hawkshot
		add_pressed_claim = title:c_birchwhite
	}
}

#Stefen and Rihouen dad
50004 = {
	name = "Deroch"
	religion = court_of_nerat
	culture = roilsardi
	
	968.1.30 = {
		birth = yes
	}
}

#Stefen and Rihouen mum
50005 = {
	name = "Iudlouguen"
	religion = court_of_nerat
	culture = roilsardi
	female = yes
	
	970.1.30 = {
		birth = yes
	}
}

#Diaco father
50006 = {
	name = "Drosuk"
	religion = court_of_nerat
	culture = roilsardi
	
	966.12.12 = {
		birth = yes
	}
	990.1.3 = {
		add_spouse = 50007
	}
	999.5.10 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

#Diaco mother
50007 = {
	name = "Borena"
	religion = court_of_nerat
	culture = roilsardi
	female = yes
	
	970.5.29 = {
		birth = yes
	}
	990.1.3 = {
		add_spouse = 50006
	}
}

#Jakub of Cronesford
50008 = {
	name = "Jakub"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_cronesford #TODO
	
	trait = content
	trait = trusting
	trait = fickle
	trait = education_learning_3

	964.5.12 = {
		birth = yes
	}
	985.2.6 = {
		add_spouse = 50009
	}
}

#Wife of Jakub of Cronesford
50009 = {
	name = "Winidilda"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_cronesford #TODO
	female = yes
	
	trait = trusting

	964.5.12 = {
		birth = yes
	}
	985.2.6 = {
		add_spouse = 50008
	}
}

#Child 1 of Jakub of Cronesford
50010 = {
	name = "Jakub"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_cronesford #TODO
	
	trait = content
	trait = chaste
	trait = fickle
	trait = education_stewardship_2
	
	father = 50008
	mother = 50009
	
	986.11.8 = {
		birth = yes
	}
	1006.3.13 = {
		add_spouse = 50014
	}
}

#Child 2 of Jakub of Cronesford
50012 = {
	name = "Stepanoz"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_cronesford #TODO
	
	father = 50008
	mother = 50009
	
	987.6.30 = {
		birth = yes
	}
}

#Child 3 of Jakub of Cronesford
50013 = {
	name = "Rycheza"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_cronesford #TODO
	female = yes
	
	father = 50008
	mother = 50009
	
	
	989.1.5 = {
		birth = yes
	}
}

#Wife of Child 1 of Jakub of Cronesford
50014 = {
	name = "VE_ra"
	religion = cult_of_nerat
	culture = milcori
	female = yes
	
	989.1.5 = {
		birth = yes
	}
	1006.3.13 = {
		add_spouse = 50010
	}
}

#Child of Child 1 of Jakub of Cronesford
50015 = {
	name = "FrantiS_ek"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_cronesford
	father = 50010
		
	1009.5.1 = {
		birth = yes
	}
}

#Guichard of Arannen
50016 = {
	name = "Guichard"
	religion = court_of_nerat
	culture = roilsardi
	dynasty = dynasty_arannen #TODO
	mother = roilsardis_0014
	father = milcori_0001
	
	trait = brave
	trait = compassionate
	trait = arrogant
	trait = strong
	trait = lifestyle_blademaster
	trait = education_martial_4
	
	973.3.18 = {
		birth = yes
		add_trait_xp = {
			trait = lifestyle_blademaster
			value = 50
		}
	}
}

#Cixillo countess of Endersby
50017 = {
	name = "Cixillo"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_endersby #TODO
	female = yes
	
	trait = brave
	trait = temperate
	trait = calm
	trait = beauty_good_1
	trait = education_stewardship_1
		
	1002.5.14 = {
		birth = yes
	}
	1021.12.1 = {
		add_spouse = 50028
		effect = {
			set_relation_soulmate = character:50028
		}
	}
}

#Trutgaud
50018 = {
	name = "Trutgaud"
	religion = court_of_nerat
	culture = lenco_damerian
	dynasty = dynasty_wesdam
	
	trait = brave
	trait = temperate
	trait = calm
	trait = disinherited
	trait = education_stewardship_1
	
	father = damerian7010

	974.3.8 = {
		birth = yes
	}
	992.11.30 = {
		add_spouse = 50019
	}
}

#Wife of Trutgaud
50019 = {
	name = "Marianna"
	religion = cult_of_the_dame
	culture = lenco_damerian
	female = yes

	976.12.7 = {
		birth = yes
	}
	992.11.30 = {
		add_spouse = 50018
	}
}

#Daughter 1 of Trutgaud
50020 = {
	name = "Eilisabet"
	religion = cult_of_the_dame
	culture = lenco_damerian
	dynasty = dynasty_wesdam #TODO
	female = yes
	
	father = 50018
	mother = 50019


	993.7.7 = {
		birth = yes
	}
}

#Daughter 2 of Trutgaud
50021 = {
	name = "Claudia"
	religion = cult_of_the_dame
	culture = lenco_damerian
	dynasty = dynasty_wesdam #TODO
	female = yes
	
	father = 50018
	mother = 50019

	998.12.12 = {
		birth = yes
	}
}

#Iveri of Brinkwick
50023 = {
	name = "Iveri"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_wispsiren #TODO

	trait = content
	trait = zealous
	trait = compassionate
	trait = celibate
	trait = education_diplomacy_3

	968.4.15 = {
		birth = yes
	}
	988.6.24 = {
		add_spouse = 50024
	}
}

#Iveri of Brinkwick Wife
50024 = {
	name = "Sisenanda"
	religion = cult_of_nerat
	culture = milcori
	female = yes

	965.6.1 = {
		birth = yes
	}
	988.6.24 = {
		add_spouse = 50023
	}
	1002.5.6 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

#Nerse of Murkglade
50025 = {
	name = "Nerse"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_wispsiren #TODO
	
	trait = arrogant
	trait = wrathful
	trait = gluttonous
	trait = education_martial_1
	
	father = 50023
	mother = 50024

	994.1.5 = {
		birth = yes
	}	
}

#Ia of Murkglade
50026 = {
	name = "Ia"
	religion = cult_of_nerat
	culture = milcori
	dynasty = dynasty_wispsiren #TODO
	female = yes
	
	trait = paranoid
	trait = stubborn
	trait = temperate
	trait = education_stewardship_3
	
	father = 50023
	mother = 50024

	1002.3.14 = {
		birth = yes
		effect = {
			set_relation_guardian = character:50027
		}
	}
	1010.1.5 = {
		effect = {
			set_relation_bully = character:50025
		}
	}
	1020.3.14 = {
		effect = {
			remove_relation_guardian = character:50027
			set_relation_friend = character:50027
			
			remove_relation_bully = character:50025
			set_relation_rival = character:50025
			add_opinion = {
				modifier = childhood_bully
				target = character:50027
			}
		}
	}
}

#Ordonno 
50027 = {
	name = "Ordonno"
	religion = cult_of_nerat
	culture = milcori
	
	trait = temperate
	trait = diligent
	trait = compassionate
	trait = education_stewardship_3

	976.7.2 = {
		birth = yes
	}
	
	1020.3.14 = {
		employer = 50026
	}
}

#Derrien of Arannen
50028 = {
	name = "Derrien"
	religion = court_of_nerat
	culture = roilsardi
	dynasty = dynasty_arannen #TODO
	
	trait = brave
	trait = temperate
	trait = arrogant
	trait = education_martial_3
	
	father = 50016
	
	1001.9.9 = {
		birth = yes
	}
	1021.12.1 = {
		add_spouse = 50017
	}
}

milcori_0001 = {
	name = "Arganto"
	religion = cult_of_nerat
	culture = milcori
	
	trait = brave
	trait = content
	trait = arrogant
	trait = education_martial_2
	trait = adventurer
	
	948.9.17 = {
		birth = yes
	}
	972.4.15 = {
		add_matrilineal_spouse = roilsardis_0014
	}
	1001.3.4 = {
		death = yes
	}
}