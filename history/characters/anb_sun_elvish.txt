﻿
sun_elvish0001 = { # Jaher
	name = "Jaher"
	dna = 23_jaher
	dynasty = dynasty_jaherzuir #Jaherzuir
	religion = "bulwari_sun_cult"
	culture = "sun_elvish"
	
	diplomacy = 5
	martial = 10
	stewardship = 5
	intrigue = 8
	learning = 7
	prowess = 14
	
	trait = race_elf
	trait = education_martial_4
	trait = ambitious
	trait = arrogant
	trait = impatient
	trait = intellect_good_1
	trait = physique_good_3
	trait = beauty_good_2
	trait = magical_affinity_1
	trait = sun_reborn
	
	791.6.1 = {	#June 1st, aka the first day of our month Suren aka Sun's Start - almost as if hes the chosen one
		birth = yes
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
		effect = {
			add_character_flag = has_scripted_appearance
			add_character_flag = no_headgear
			
			set_artifact_rarity_illustrious = yes
			create_artifact = {
				name = dinatoldir_name
				description = dinatoldir_description
				type = spear
				template = dinatoldir_template
				visuals = spear # TODO
				wealth = scope:wealth
				quality = scope:quality
				history = {
					type = created_before_history
					recipient = character:sun_elvish0001
				}
				modifier = dinatoldir_modifier
				save_scope_as = newly_created_artifact_1
			}
			scope:newly_created_artifact_1 = {
				add_artifact_history = {
					recipient = character:sun_elvish0001
					type = inherited
					date = 801.6.1
				}
				set_variable = { name = historical_unique_artifact value = yes }
				set_variable = is_dinatoldir # Do not add this for all artifacts, only those with on_action effects need it
			}
			set_artifact_rarity_illustrious = yes
			create_artifact = {
				name = flag_of_the_diranhria_name
				description = flag_of_the_diranhria_description
				type = wall_big
				template = flag_of_the_diranhria_template
				visuals = banner
				visuals_source = title:e_bulwar # placeholder until we have something
				wealth = scope:wealth
				quality = scope:quality
				history = {
					type = created_before_history
					recipient = character:sun_elvish0001
				}
				modifier = flag_of_the_diranhria_modifier
				save_scope_as = newly_created_artifact_2
				decaying = no
			}
			scope:newly_created_artifact_2 = {
				set_variable = { name = historical_unique_artifact value = yes }
			}
		}
	}
	
	992.5.1 = {
		add_spouse = sun_elvish0004 # Erelmis the Chaste
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0014 # Shaeranni the Dancer
	}
	
	1005.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0010 # Ariathra the Beautiful
	}
	
	1015.09.07 = {
		add_spouse = 168 # Lanahari ta'Luneteín
	}

	1127.12.21 = {
		death = {
			death_reason = death_murder
		}
	}
}

sun_elvish0002 = { # Jaerel
	name = "Jaerel"
	dynasty = dynasty_jaherzuir #Jaherzuir
	religion = "bulwari_sun_cult"
	culture = "sun_elvish"
	
	trait = race_elf
	trait = education_diplomacy_2
	trait = just
	trait = compassionate
	trait = trusting
	trait = sun_reborn_descendant

	father = sun_elvish0001 # Jaher
	mother = sun_elvish0004 # Erelmis the Chaste

	995.5.1 = {
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
		birth = yes
	}

	1137.12.21 = {
		death = {
			death_reason = death_murder
		}
	}
}

sun_elvish0003 = { # Elisar
	name = "Elisar"
	dynasty = dynasty_jaherzuir #Jaherzuir
	religion = "bulwari_sun_cult"
	culture = "sun_elvish"
	
	trait = race_elf
	trait = education_martial_4
	trait = wrathful
	trait = paranoid
	trait = sadistic
	trait = lunatic_1
	trait = aggressive_attacker
	trait = sun_reborn_descendant
	
	father = sun_elvish0001 # Jaher
	mother = sun_elvish0014 # Shaeranni the Dancer
	
	1004.7.8 = {
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
		birth = yes
	}

	1173.8.1 = {
		death = yes
	}
}

sun_elvish0004 = { # Erelmis the Chaste, first wife of Jaher, mother of Jaerel
	name = "Erelmis"
	female = yes
	religion = "bulwari_sun_cult"
	culture = "sun_elvish"
	
	trait = race_elf_dead
	trait = beauty_good_3
	trait = chaste
	trait = humble
	trait = compassionate
	
	792.2.1 ={
		birth = yes
	}
	
	992.5.1 = {
		add_spouse = sun_elvish0001 # Jaher
		give_nickname = nick_the_chaste
	}
	
	1001.9.1 = {
		death = yes
	}
}

sun_elvish0005 = { # Denarion Denarzuir, governor of Corvuria
	name = "Denarion"
	dna = 113_denarion_denarzuir
	dynasty = dynasty_denarzuir	#Denarzuir
	religion = "bulwari_sun_cult"
	culture = "sun_elvish"
	
	diplomacy = 5
	martial = 7
	stewardship = 10
	intrigue = 4
	learning = 4
	prowess = 9	
	
	trait = race_elf
	trait = education_stewardship_4
	trait = arrogant
	trait = temperate
	trait = diligent
	
	783.6.2 = {
		birth = yes
		add_character_flag = has_scripted_appearance
	}
}

sun_elvish0006 = { # Eledas I Sarelzuir, governor of Sareyand
	name = "Eledas"
	dynasty = dynasty_sarelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	trait = education_learning_3
	trait = diligent
	trait = arrogant
	trait = generous
	
	925.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0009
	}
}

sun_elvish0007 = { # Eledas II Sarelzuir
	name = "Eledas"
	dynasty = dynasty_sarelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	trait = twin
	
	father = sun_elvish0006
	mother = sun_elvish0009
	
	1019.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0008 = { # Panoril Sarelzuir
	name = "Panoril"
	dynasty = dynasty_sarelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	trait = twin
	
	father = sun_elvish0006
	mother = sun_elvish0009
	
	1019.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0009 = { # Varilla, wife of Eledas I, mother of Eledas II
	name = "Varilla"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	878.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0006
	}
}

sun_elvish0010 = { # Ariathra the Beautiful, sister of Eledas I, third wife of Jaher
	name = "Ariathra"
	dynasty = dynasty_sarelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	trait = beauty_good_3
	
	950.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1005.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0001 # Jaher
	}
}

# add a parent for Eledas and Ariathra

sun_elvish0011 = { # Dorendor Olorzuir, General of Jaher
	name = "Dorendor"
	dynasty = dynasty_olorzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	trait = education_martial_4
	trait = brave
	trait = patient
	trait = wrathful
	trait = military_engineer
	# trait = giant # marked with ? in the doc
	
	878.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
		# rezangal shield artifact
	}
	
	1022.1.1 = {
		employer = sun_elvish0001
	}
}

sun_elvish0012 = { # Vulzin "the Red" Vulzinzuir, future governor of Elizna
	name = "Vulzin"
	dynasty = dynasty_vulzinzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education_stewardship_
	trait = arbitrary
	trait = vengeful
	trait = content
	
	830.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
		give_nickname = nick_the_red
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0013
	}
}

sun_elvish0013 = { # Liandiel, wife of Vulzin
	name = "Liandiel"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	900.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0012
	}
}

sun_elvish0014 = { # Shaeranni the Dancer, sister of Vulzin, second wife of Jaher, Elisar's mother
	name = "Shaeranni"
	dynasty = dynasty_vulzinzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	trait = fickle
	trait = impatient
	trait = gregarious
	trait = lifestyle_reveler
	
	904.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
		give_nickname = nick_the_dancer
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0001 # Jaher
		
		add_trait_xp = {
			trait = lifestyle_reveler
			value = 100
		}
	}
}

# add a parent for Vulzin and Shaeranni

sun_elvish0015 = { # Amarien Amarienzuir, governor of Imulushes, Jaher's best friend
	name = "Amarien"
	dynasty = dynasty_amarienzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	trait = education_martial_4
	trait = brave
	trait = cynical
	trait = shy
	
	789.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		set_relation_best_friend = character:sun_elvish0001 # Jaher
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0016
	}
}

sun_elvish0016 = { # Artorian, husband of Amarien
	name = "Artorian"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	841.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0015
	}
}

sun_elvish0017 = { # Alvarion Alvazuir
	name = "Alvarion"
	dynasty = dynasty_alvazuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education_intrigue_
	trait = lustful
	trait = arrogant
	trait = calm
	
	838.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0021
	}
}

sun_elvish0018 = { # Elaressa Alvazuir, future governor of Azka-Sur
	name = "Elaressa"
	dynasty = dynasty_alvazuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education_stewardship_
	trait = content
	trait = zealous
	trait = generous
	
	father = sun_elvish0017
	mother = sun_elvish0021
	
	911.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0019 = { # Darastarion Alvazuir
	name = "Darastarion"
	dynasty = dynasty_alvazuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0017
	mother = sun_elvish0021
	
	1007.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0020 = { # Ultarion Alvazuir
	name = "Ultarion"
	dynasty = dynasty_alvazuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0017
	mother = sun_elvish0021
	
	1018.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0021 = { # Alarawel, wife of Alvarion
	name = "Alarawel"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	810.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0017
	}
}

sun_elvish0022 = { # Gelmonias Dalzuir, future governor of Dalarand
	name = "Gelmonias"
	dynasty = dynasty_dalzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	866.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0024
	}
}

sun_elvish0023 = { # Darastarion Dalzuir
	name = "Darastarion"
	dynasty = dynasty_dalzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0022
	mother = sun_elvish0024
	
	1004.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0024 = { # Panoril, wife of Gelmonias
	name = "Panoril"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	840.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0022
	}
}

sun_elvish0025 = { # Varamel Varamzuir, Count of Varamar (vassal of Gelmonias)
	name = "Varamel"
	dynasty = dynasty_varamzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education

	841.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0027
	}
}

sun_elvish0026 = { # Galindil Varamzuir
	name = "Galindil"
	dynasty = dynasty_varamzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0025
	mother = sun_elvish0027
	
	1011.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0027 = { # Eranil, wife of Varamel
	name = "Eranil"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	874.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0025
	}
}

sun_elvish0028 = { # Irrlion Irrliazuir, father of Taelarios (not born), count of Irrliam
	name = "Irrlion"
	dynasty = dynasty_irrliazuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	trait = zealous
	trait = humble
	trait = lazy
	
	800.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0029
	}
}

sun_elvish0029 = { # Imariel, wife of Irrlion
	name = "Imariel"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	840.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0028
	}
}

sun_elvish0030 = { # Uyel Uyelzuir, governor of Re’uyel
	name = "Uyel"
	dynasty = dynasty_uyelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education_stewardship_
	trait = arrogant
	trait = greedy
	trait = deceitful
	
	842.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0031
	}
}

sun_elvish0031 = { # Kalinael, husband of Uyel
	name = "Kalinael"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	784.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0030
	}
}

sun_elvish0032 = { # Kalinael Uyelzuir
	name = "Kalinael"
	dynasty = dynasty_uyelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education_intrigue_
	trait = deceitful
	trait = ambitious
	trait = calm
	
	father = sun_elvish0031
	mother = sun_elvish0030
	
	995.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0033 = { # Veharia Uyelzuir
	name = "Veharia"
	dynasty = dynasty_uyelzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0031
	mother = sun_elvish0030
	
	1021.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0034 = { # Veharia Lezuir, governor of Ginerdu
	name = "Veharia"
	dynasty = dynasty_lezuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	899.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0035
	}
}

sun_elvish0035 = { # Talanor, lowborn, husband of Veharia
	name = "Talanor"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	912.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0034
	}
}

sun_elvish0036 = { # Firinar Aralzuir, governor of Kalib
	name = "Firinar"
	dynasty = dynasty_aralzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	912.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0037
	}
}

sun_elvish0037 = { # Iztralania, lowborn, wife of Firinar
	name = "Iztralania"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	847.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0036
	}
}

sun_elvish0038 = { # Thelrion Nestezuir, future governor of Setadazar
	name = "Thelrion"
	dynasty = dynasty_nestezuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	850.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0039
	}
}

sun_elvish0039 = { # Lezlindel, lowborn, wife of Thelrion
	name = "Lezlindel"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	899.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0038
	}
}

sun_elvish0040 = { # Thranduir Tirenzuir, governor of Busilar
	name = "Thranduir"
	dynasty = dynasty_tirenzuir
	religion = bulwari_sun_cult
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	901.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0041
	}
	
	1021.12.31 = {
		set_realm_capital = title:c_port_jaher
	}
}

sun_elvish0041 = { # Alarawel, lowborn, wife of Thranduir
	name = "Alarawel"
	# lowborn
	religion = bulwari_sun_cult
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	921.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0040
	}
}
